#ifndef _POSIX_NETDB_H_
#define _POSIX_NETDB_H_

#ifdef HAVE_LWIP

#include <lwipopts.h>
#include <lwip/netdb.h>

struct hostent *gethostbyname(const char *name);
int gethostbyname_r(const char *name, struct hostent *ret, char *buf, size_t buflen, struct hostent **result, int *h_errnop);
void freeaddrinfo(struct addrinfo *ai);
int getaddrinfo(const char *nodename, const char *servname, const struct addrinfo *hints, struct addrinfo **res);

int getnameinfo(const struct sockaddr *sa, socklen_t salen, char *host, size_t hostlen, char *serv, size_t servlen, int flags);
const char *gai_strerror(int error_code);

char *inet_ntop(int af, void *src, char *dst, socklen_t size);
int   inet_pton(int af, const char *src, void *dst);

struct sockaddr_storage {
  int ss_family;  
  struct sockaddr_in v4;
};

#else

struct hostent {
    char *h_addr;
};
#define gethostbyname(buf) NULL

#endif

#endif /* _POSIX_NETDB_H_ */
